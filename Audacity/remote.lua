local kb = libs.keyboard;
local script = libs.script;
local timer = libs.timer;

--@help Start new channel
actions.start_recording = function()
    kb.press("r");
    layout.play_stop.icon = "stop";
    layout.play_stop.color = "#e7b757";
end

--@help Append recording
actions.append_recording = function()
    kb.stroke("shift", "r");
    layout.play_stop.icon = "stop";
    layout.play_stop.color = "#e7b757";
    timer.timeout(function()
--        print("wibble");
    end, 200);
    kb.stroke("ctrl", "m");
    kb.text("Take");
    kb.press("return");
end

--@help Pause
actions.pause = function()
    kb.press("p");
end

--@help Play/stop
actions.play_stop = function()
    kb.press("space");
    layout.play_stop.icon = "play";
    layout.play_stop.color = "#75d570";
end

--@help Mute mic
actions.mute_mic = function()
    script.default("pacmd set-source-mute " .. settings.device_name .. " 1");
end

--@help Unmute mic
actions.unmute_mic = function()
    script.default("pacmd set-source-mute " .. settings.device_name .. " 0");
end

--@help Add marker
actions.add_marker = function()
    kb.stroke("ctrl", "m");
    kb.text("Mark");
    kb.press("return");
end
